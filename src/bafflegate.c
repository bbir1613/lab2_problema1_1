/* ****************************************************** */
/* State machine generated by Red State Machine generator */
/* Tue Nov 07 01:22:47 EET 2017                           */
/* This file was automatically generated and will be      */
/* OVERWRITTEN if regenerated.                            */
/*  o Do not make changes to this file directly.          */
/*  o Do define your actions functions in                 */
/*     bafflegate_actions.c                               */
/* ****************************************************** */
#include "bafflegate.h"

static const state initialState = S_STATE_1;   /* The starting state : state 1 */
static state currentState = S_STATE_1;   /* Initialize the current state. */

static const inputs * inRef; /* Const pointer to the last input sent to the dispatch function */
static outputs * outRef; /* Pointer to the outputs sent to the dispatch function */

void preload(outputs * out)
{
    out->output_1 = 30000;
}

void bafflegate_dispatch(inputs theInputSignal, outputs * out)
{
    /* The inputs */
    int buttonPressed = theInputSignal.buttonPressed;  /* generic variable buttonPressed setting type from input source. */
    int input_1 = theInputSignal.input_1;  /* generic variable input 1 setting type from input source. */
    int input_2 = theInputSignal.input_2;  /* generic variable input 2 setting type from input source. */

    /* The outputs */
    int output_1 = out->output_1;  /*  output variable output 1 (type taken from variable's source field - not checked) */

    inRef = &theInputSignal; /* To give functions access to the input signal via const inputs * getInput() */
    outRef = out; /* To give functions access to the outputs via outputs * getOutput() */

    /* Check for the reset signal: RESET */
    if(input_1) {
        preload(out);
        currentState = initialState;
        return;
    }
    /* Perform any actions and state transitions for the current state and input */
    switch(currentState) {
        case S_STATE_1:
            /* !buttonPressed */
            if(!buttonPressed) {
                nextState();  /* Action: nextState */
                resetButton();  /* Action: resetButton */
                currentState = S_STATE_2;
                return;
            }
            break;
        case S_STATE_2:
            /* (input 2 == output 1) */
            if((input_2 == output_1)) {
                normal();  /* Action: normal */
                currentState = S_STATE_1;
                return;
            }
            /* !(input 2 == output 1) */
            if(!(input_2 == output_1)) {
                inc();  /* Action: inc */
                return;
            }
            break;
        default:
            /* ***************** NOTE ******************** */
            /* If the current state is not recognized      */
            /* the state machine will stay in that unknown */
            /* state until it receives the reset signal    */
            /* ******************************************* */
            break;
    }
}
int getState()
{
    return currentState;
}

const inputs * getInput()
{
    return inRef;
}

outputs * getOutput()
{
    return outRef;
}

