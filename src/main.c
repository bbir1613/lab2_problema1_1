//*****************************************************************************
//   +--+
//   | ++----+
//   +-++    |
//     |     |
//   +-+--+  |
//   | +--+--+
//   +----+    Copyright (c) 2011 Code Red Technologies Ltd.
//
// LED flashing SysTick application for LPCXPresso11U14 board
//
// Software License Agreement
//
// The software is owned by Code Red Technologies and/or its suppliers, and is
// protected under applicable copyright laws.  All rights are reserved.  Any
// use in violation of the foregoing restrictions may subject the user to criminal
// sanctions under applicable laws, as well as to civil liability for the breach
// of the terms and conditions of this license.
//
// THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
// OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
// USE OF THIS SOFTWARE FOR COMMERCIAL DEVELOPMENT AND/OR EDUCATION IS SUBJECT
// TO A CURRENT END USER LICENSE AGREEMENT (COMMERCIAL OR EDUCATIONAL) WITH
// CODE RED TECHNOLOGIES LTD.
//
//*****************************************************************************

#ifdef __USE_CMSIS
#include "LPC11Uxx.h"
#endif


#include "LPC11Uxx.h"

#include "gpio.h"

#include <time.h>
#include "bafflegate.h"

#define ON 0
#define OFF 1

#define IN 0
#define OUT 1

void delay (){
	for(int i=0; i<2*500000;i+=2)
	{
	//	printf("delay~~~");
	}

}

void setPins(){
 	     //0 intrare, 1 iesire
	GPIOSetDir(0, 9, OUT); //LED este de iesire
	GPIOSetDir(0, 1, OUT); //LED este de iesire
	GPIOSetDir(0, 2, OUT); //LED este de iesire
	GPIOSetDir(0, 3, OUT); //LED este de iesire
	GPIOSetDir(0, 4, OUT); //LED este de iesire
	GPIOSetDir(0, 5, OUT); //LED este de iesire
	GPIOSetDir(0, 7, OUT); //LED este de iesire

    //0 intrare, 1 iesire
	GPIOSetDir(0, 21, IN); // Btn este de intrare
	GPIOSetDir(1, 23, IN); // Btn este de intrare
	GPIOSetDir(0, 11, IN); // Btn este de intrare
}

void afiseazaCifra(int status){
	switch(status){
		case OFF:
			GPIOSetBitValue(0,9, OFF);
			GPIOSetBitValue(0,1, OFF);
			GPIOSetBitValue(0,2, OFF);
			GPIOSetBitValue(0,3, OFF);
			GPIOSetBitValue(0,4, OFF);
			GPIOSetBitValue(0,5, OFF);
			GPIOSetBitValue(0,7, OFF);
			break;
		default:
			GPIOSetBitValue(0,9, ON);
			GPIOSetBitValue(0,1, ON);
			GPIOSetBitValue(0,2, ON);
			GPIOSetBitValue(0,3, ON);
			GPIOSetBitValue(0,4, ON);
			GPIOSetBitValue(0,5, ON);
			GPIOSetBitValue(0,7, ON);
			break;
	}
}
inputs input;

int main(void) {

	  GPIOInit();
	  setPins();
	  afiseazaCifra(ON);
	  input.buttonPressed = 0;
	  input.input_1 = 0;
	  input.input_2 = 0;
	  outputs out;
	  /* Loop forever */
	  while (1){
		  input.buttonPressed = GPIOGetPinValue(0,21);
		  bafflegate_dispatch(input,&out);
	  } //end while
}

