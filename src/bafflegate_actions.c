/* edit this file to implement action functions. 
  You can also regenerate this file to fill with auto generated stubs,
 but any existing content will be OVERWRITTEN! */
#include "bafflegate_actions.h"
#include "bafflegate.h"

void nextState(){
		GPIOSetBitValue(0,9, 1);
		GPIOSetBitValue(0,1, 1);
		GPIOSetBitValue(0,2, 1);
		GPIOSetBitValue(0,3, 1);
		GPIOSetBitValue(0,4, 1);
		GPIOSetBitValue(0,5, 1);
		GPIOSetBitValue(0,7, 1);
 }

void resetButton(){
	outputs* out = getOutput();
	out->output_1 = 30000;
}
void inc(){
	outputs* out = getOutput();
	out->output_1 -=1;
}
void normal(){
	GPIOSetBitValue(0,9, 0);
	GPIOSetBitValue(0,1, 0);
	GPIOSetBitValue(0,2, 0);
	GPIOSetBitValue(0,3, 0);
	GPIOSetBitValue(0,4, 0);
	GPIOSetBitValue(0,5, 0);
	GPIOSetBitValue(0,7, 0);
}
